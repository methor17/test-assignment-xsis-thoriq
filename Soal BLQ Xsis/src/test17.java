import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class test17 {

    static int countingValleys(int n, String s) {
        // initialize variabel
        int valley = 0;
        int level = 0;
        
        // kita gunakan looping untuk mengecek level permukaan
        for(char current:s.toCharArray()){
            // jika U maka level tambah + 1
            if(current == 'N') ++level;
            // jika D maka level tambah - 1
            if(current == 'T') --level;
            
            
            // kita berfokus pada level permukaan 
            // karena setiap lembah pasti akan diakhiri dengan
            // level permukaan air laut dan Langkah 'Naik'
            if(level == 0 && current == 'N'){
                valley++;  // jika benar maka valley tambah.
            }
            
            
        }
        
        return valley;
    }
        static int countingMount(int n, String s) {
        // initialize variabel
        int mount = 0;
        int level = 0;
        
        // kita gunakan looping untuk mengecek level permukaan
        for(char current:s.toCharArray()){
            // jika U maka level tambah + 1
            if(current == 'N') ++level;
            // jika D maka level tambah - 1
            if(current == 'T') --level;
            
            
            // kita berfokus pada level permukaan 
            // karena setiap lembah pasti akan diakhiri dengan
            // level permukaan air laut dan Langkah 'Naik'
            if(level == 0 && current == 'T'){
                mount++;
            }
            
            
        }
        
        return mount;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Input nilai N : ");
        int n = in.nextInt();
        String s = in.next();
        
        int result = countingValleys(n, s);
        int result1 = countingMount(n, s);
        
        System.out.println("Gunung = " + result1);
        System.out.println("Lembah = " + result);
        in.close();
    }
}