import java.util.*;

public class test8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		String tampil = "";
		
		System.out.println("Masukan angka : ");
		String[] arrayangka = input.nextLine().split(" ");
		int n = arrayangka.length;
		int arrayderetangka[] = new int[n];
		
		
		//String ke Int
		for (int i = 0; i < n; i++) {
			arrayderetangka[i] = Integer.parseInt(arrayangka[i]);
		}
		Arrays.sort(arrayderetangka);
		Arrays.sort(arrayangka);
		
		//mengurutkan Angka
		for (int i = 0; i < n; i++) {
			tampil += arrayangka[i] + " ";
		}
	

		int total4terkecil = 0;
		int total4terbesar = 0;
		
		//Perhitungan Angka
		for (int i = 0; i < 4; i++) {
			total4terkecil += arrayderetangka[i];
			total4terbesar += arrayderetangka[n - 1 - i];
		}

		System.out.println(" ");
		System.out.println(" ");
		System.out.println("Angka yang telah diurut : " + tampil);
		System.out.println("Total 4 Angka Terkecil  : " + total4terkecil);
		System.out.println("Total 4 Angka Terbesar  : " + total4terbesar);
	}

}
