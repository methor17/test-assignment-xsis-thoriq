import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.lang.*;
import java.time.*;
import java.util.*;
import java.text.*;
import java.time.*;
import java.time.format.DateTimeParseException;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;


public class test3 {
    
    public static void main(String[] args) throws Exception {
        
       Scanner scanner = new Scanner(System.in);

        System.out.println("Masukan tanggal masuk (dd/MM/yyyy HH:mm:ss):");
        String tanggalMasuk = scanner.nextLine();

         System.out.println("Masukan tanggal keluar (dd/MM/yyyy HH:mm:ss):");
        String tanggalkeluar = scanner.nextLine();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");

        
        LocalDateTime waktuPertama = LocalDateTime.parse(tanggalMasuk, dtf);

        LocalDateTime waktuKedua = LocalDateTime.parse(tanggalkeluar,dtf);

        // Menghitung selisih waktu
        Duration selisih = Duration.between(waktuPertama, waktuKedua);
        Duration waktu8Jam = Duration.ofHours(8);
        Duration waktu24Jam = Duration.ofHours(24);
        int jumlahBayar = 0;


        // Mendapatkan hasil dalam jam, menit, dan detik
        long jamSelisih = selisih.toHours();
        long menitSelisih = selisih.toMinutes();
        long detikSelisih = selisih.getSeconds();

        if(selisih.getSeconds() <= waktu8Jam.getSeconds()){
            jumlahBayar =(int) jamSelisih * 1000 ;
        } else if (selisih.getSeconds()<= waktu24Jam.getSeconds()) {
            jumlahBayar = 8000;
        } else{
            jumlahBayar = 15000 + ((int) jamSelisih - 24)*1000;
        }
        System.out.println("Jumlah bayar = "+ jumlahBayar);
        System.out.println("Selisih waktu: " + jamSelisih + " jam, " + menitSelisih + " menit, " + detikSelisih + " detik");
        
    }
}
