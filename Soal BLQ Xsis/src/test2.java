import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.lang.*;
import java.time.*;
import java.util.*;
import java.text.*;
import java.time.*;
import java.time.format.DateTimeParseException;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;


public class test2 {
    public static void main(String[] args)  {
        
        String[] namaBuku = {"A", "B", "C", "D"};
        int[] durasiPeminjaman = {14, 3, 7, 7};
        int jumlahDenda;

      Scanner input1 = new Scanner(System.in);
      System.out.println("Masukan tanggal mulai (dd/MM/yyyy) :");
      String waktuPertama = input1.nextLine();

      Scanner input2 = new Scanner(System.in);
      System.out.println("Masukan tanggal berakhir (dd/MM/yyyy) :");
      String waktuKedua = input2.nextLine(); 
      
      DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd MM yyyy");
        for(int i = 0; i < 4 ; i++){
            try {
             LocalDate date1     = LocalDate.parse(waktuPertama,dtf);
             LocalDate date2     = LocalDate.parse(waktuKedua,dtf);
              int daysBetween = (int) ChronoUnit.DAYS.between(date1, date2);

              int selisihHari = daysBetween - durasiPeminjaman[i];
              // System.out.println ("Days: " + daysBetween);


              //menghitung denda
              if(selisihHari <= 0){
                jumlahDenda = 0;
                selisihHari = 0;
              }else{
                jumlahDenda = selisihHari * 100;
              }
            System.out.println("Buku " + namaBuku[i] + ":");
                System.out.println("Tanggal Pengembalian: " + date2);
                System.out.println("Keterlambatan: " + selisihHari + "hari");
                System.out.println("Jumlah Denda: " + jumlahDenda);
                System.out.println();


            } catch (Exception e) {
                e.printStackTrace();
                 }
        }
    }
}
