import java.util.Scanner;
import java.util.Arrays;
import java.util.HashMap;

// Time Complexity: O(n log n) due to sorting
public class test7 {
    public static void main(String[] args) {
        /* Save input */
       Scanner input = new Scanner(System.in);
		System.out.print("Masukkan data : ");
		String[] data = input.nextLine().split(" ");
		int n = data.length;
		int[] dataInt = new int[n];

		for (int i = 0; i < n; i++) {
			dataInt[i] = Integer.parseInt(data[i]);
		}
		// mean
		float mean = 0;
		for (int i = 0; i < n; i++) {
			mean += dataInt[i];
		}
		System.out.println("Mean = " + mean / n);

		// Median
		System.out.print("Median = ");
		if (n % 2 == 1) {
			Arrays.sort(dataInt);
			int median = dataInt[n / 2];
			System.out.println(median);
		} else {
			Arrays.sort(dataInt);
			float median = (float) ((dataInt[n / 2] + dataInt[(n / 2) - 1]) / 2.0);
			System.out.println(median);
		}

		// Modus
                System.out.print("Modus = ");
		int maxCount = 0;
		int maxValue = 0;

		for (int i = 0; i < n; i++) {
			int count = 0;
			for (int j = 0; j < n; j++) {
				if (dataInt[i] == dataInt[j]) {
					count++;
				}
				if (count > maxCount) {
					maxCount = count;
					maxValue = dataInt[i];
				}
			}
		}
		System.out.println(maxValue);
		input.close();
    }
}